from django.http import HttpResponse


html_code = """
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>TeachMeSkills</title>
  </head>
  <body>
    <h1>TeachMeSkills</h1>
    <h3>Дипломный проект Бояревича О.Н.</h3>
    <p>
    Использованные инструменты:<br>
    - GitLab (CI/CD)<br>
    &nbsp;&nbsp;&nbsp;<a href="https://gitlab.com/teachmeskills_project">https://gitlab.com/teachmeskills_project</a><br>
    - Terraform<br>
    - Ansible<br>
    - AWS (EC2, VPC, Route53, S3)<br>
    - Jenkins<br>
    &nbsp;&nbsp;&nbsp;<a href="https://jenkins.alehbayarevich.art">https://jenkins.alehbayarevich.art</a><br>
    - Docker<br>
    - Python<br>
    - Django<br>
     &nbsp;&nbsp;&nbsp;<a href="https://alehbayarevich.art/admin">https://alehbayarevich.art/admin</a><br>
    - Bash<br>
    </p>

 </body>
</html>
"""


def index(request):
    # print(dir(request))
    return HttpResponse(html_code)

# class BookListView(ListView):
#     model = Book

#     def head(self, *args, **kwargs):
#         last_book = self.get_queryset().latest('publication_date')
#         response = HttpResponse(
#             # RFC 1123 date format.
#             headers={'Last-Modified': last_book.publication_date.strftime('%a, %d %b %Y %H:%M:%S GMT')},
#         )
#         return response
